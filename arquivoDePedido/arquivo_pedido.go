package arquivoDePedido

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {

	Cabecalho  Cabecalho `json:"Cabecalho"`
	Itens      []Itens   `json:"Itens"`
	Fim        Fim       `json:"Fim"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {

	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error

	for fileScanner.Scan() {

		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:1])

		if identificador == "1" {

			err = arquivo.Cabecalho.ComposeStruct(string(runes))

		} else if identificador == "2" {

			Itens := Itens{}
			Itens.ComposeStruct(string(runes))
			arquivo.Itens = append(arquivo.Itens,Itens)

		} else if identificador == "3" {

			err = arquivo.Fim.ComposeStruct(string(runes))
		}
	}
	return arquivo, err
}
