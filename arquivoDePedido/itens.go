package arquivoDePedido

import gerador_layouts_posicoes "bitbucket.org/infarma/gerador-layouts-posicoes"

type Itens struct {

	TipoRegistro    	   	 	  string `json:"TipoRegistro"`
	CodigoDoProduto  			  int64 `json:"CodigoDoProduto"`
	Quantidade				 	  int32 `json:"Quantidade"`
	Preco 	  			 		  float64 `json:"Preco"`
	Reservado            		  string `json:"Reservado"`
}

func (i *Itens) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	tamanho := len(fileContents)
	//Passo as posicoes referentes a esse struct
	if(tamanho == 20 ){

		posicaoParaValor.Posicoes = PosicoesItensIncompleto
	} else if (tamanho <= 33){

		posicaoParaValor.Posicoes = PosicoesItensComPreco
	} else if( tamanho <=80){

		posicaoParaValor.Posicoes = PosicoesItensCompleto
	}




	err = posicaoParaValor.ReturnByType(&i.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoDoProduto, "CodigoDoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Quantidade, "Quantidade")
	if err != nil {
		return err
	}

	if( tamanho > 20) {
		err = posicaoParaValor.ReturnByType(&i.Preco, "Preco")
		if err != nil {
			return err
		}
	}

	if (tamanho > 33) {
		err = posicaoParaValor.ReturnByType(&i.Reservado, "Reservado")
		if err != nil {
			return err
		}
	}

	return err

}

var PosicoesItensCompleto = map[string]gerador_layouts_posicoes.Posicao{

	"TipoRegistro":    {0,1,0},
	"CodigoDoProduto": {1,15,0},
	"Quantidade":      {15,20,0},
    "Preco": 	       {20,33,2},
    "Reservado":       {33,80,0},
}

var PosicoesItensComPreco = map[string]gerador_layouts_posicoes.Posicao{

	"TipoRegistro":    {0,1,0},
	"CodigoDoProduto": {1,15,0},
	"Quantidade":      {15,20,0},
	"Preco": 	       {20,33,2},
}

var PosicoesItensIncompleto = map[string]gerador_layouts_posicoes.Posicao{

	"TipoRegistro":    {0,1,0},
	"CodigoDoProduto": {1,15,0},
	"Quantidade":      {15,20,0},
}