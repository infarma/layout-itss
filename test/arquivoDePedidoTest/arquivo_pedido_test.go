package arquivoDePedidoTest

import (
	"fmt"
	"layout-itss/arquivoDePedido"
	"os"
	"testing"
)

func TestGetArquivoPedido(t *testing.T) {

	f, err := os.Open("../testFile/Modelo_0000000070.ped")

	if err != nil {
		t.Error("Erro ao abrir o arquivo modelo")
	}

	arq, err := arquivoDePedido.GetStruct(f)

	if err != nil {
		t.Error("Erro ao ler o arquivo")
	}

	var cabecalho arquivoDePedido.Cabecalho
	//10000000000000000701709201801701000000000000000001000050899293001645
	cabecalho.TipoRegistro = "1"
	cabecalho.CodigoDoCliente = "000000"
	cabecalho.NumeroDoPedidoDoCliente = "000000000070"
	cabecalho.DataDoPedido = 17092018
	cabecalho.CondicaoDePagamento = "0"
	cabecalho.TipoDeCompra	= "1"
	cabecalho.TipoDeRetorno	= "7"
	cabecalho.VersaoDoProgama = "0"
	cabecalho.CompraIntegral = "1"
	cabecalho.ComplementoDoPedido = "000"
	cabecalho.NumeroDoPedidoDaDistribuidora = "00000"
	cabecalho.VersaoDoProgamaPE = "00000"
	cabecalho.CodigoDaOferta = "00001"
	cabecalho.Prazo = "000"
	cabecalho.PedidoConsulta = "0"
	cabecalho.CNPJDDoCliente = "50899293001645"


	if cabecalho.TipoRegistro != arq.Cabecalho.TipoRegistro {
		t.Error("TipoRegistro não é compativel")
	}
	if cabecalho.CodigoDoCliente != arq.Cabecalho.CodigoDoCliente {
		t.Error("CodigoDoCliente não é compativel")
	}
	if cabecalho.NumeroDoPedidoDoCliente != arq.Cabecalho.NumeroDoPedidoDoCliente {
		t.Error("NumeroDoPedidoDoCliente não é compativel")
	}
	if cabecalho.DataDoPedido != arq.Cabecalho.DataDoPedido {
		t.Error("DataDoPedido não é compativel")
	}
	if cabecalho.CondicaoDePagamento != arq.Cabecalho.CondicaoDePagamento {
		t.Error("CondicaoDePagamento não é compativel")
	}
	if cabecalho.TipoDeCompra != arq.Cabecalho.TipoDeCompra {
		t.Error("TipoDeCompra não é compativel")
	}
	if cabecalho.TipoDeRetorno != arq.Cabecalho.TipoDeRetorno {
		fmt.Println("tipoderetorno", arq.Cabecalho.TipoDeRetorno )
		t.Error("TipoDeRetorno não é compativel")
	}
	if cabecalho.VersaoDoProgama != arq.Cabecalho.VersaoDoProgama {
		t.Error("VersaoDoProgama não é compativel")
	}
	if cabecalho.ComplementoDoPedido != arq.Cabecalho.ComplementoDoPedido {
		t.Error("ComplementoDoPedido não é compativel")
	}
	if cabecalho.NumeroDoPedidoDaDistribuidora != arq.Cabecalho.NumeroDoPedidoDaDistribuidora {
		t.Error("NumeroDoPedidoDaDistribuidora não é compativel")
	}
	if cabecalho.VersaoDoProgamaPE != arq.Cabecalho.VersaoDoProgamaPE {
		t.Error("VersaoDoProgamaPE não é compativel")
	}
	if cabecalho.CodigoDaOferta != arq.Cabecalho.CodigoDaOferta {
		t.Error("CodigoDaOferta não é compativel")
	}
	if cabecalho.Prazo != arq.Cabecalho.Prazo {
		t.Error("Prazo não é compativel")
	}
	if cabecalho.PedidoConsulta != arq.Cabecalho.PedidoConsulta {
		t.Error("PedidoConsulta não é compativel")
	}
	if cabecalho.CNPJDDoCliente != arq.Cabecalho.CNPJDDoCliente {
		t.Error("CNPJDDoCliente não é compativel")
	}

	var itens arquivoDePedido.Itens
	//207896658002687000100000000000000
	itens.TipoRegistro = "2"
	itens.CodigoDoProduto = 7896658002687
	itens.Quantidade = int32(10)
	itens.Preco = 0.0

	if itens.TipoRegistro != arq.Itens[0].TipoRegistro {
		t.Error("TipoRegistro não é compativel")
	}

	if itens.CodigoDoProduto != arq.Itens[0].CodigoDoProduto {
		t.Error("CodigoDoProduto não é compativel")
	}

	if itens.Quantidade != arq.Itens[0].Quantidade {
		t.Error("Quantidade não é compativel")
	}

	if itens.Preco != arq.Itens[0].Preco {
		t.Error("Preco não é compativel")
	}

	if itens.Reservado != arq.Itens[0].Reservado {
		fmt.Println("reservado do arquivo = ", arq.Itens[0].Reservado)
		t.Error("Reservado não é compativel")
	}

	var fim arquivoDePedido.Fim
	//3000000000070000020000000002
	fim.TipoRegistro = "3"
	fim.NumeroDoPedidoDoCliente = "000000000070"
	fim.QuantidadeTotalDeItens = int32(2)
	fim.QuantidadeTotalDeUnidades = 0.02

	if fim.TipoRegistro != arq.Fim.TipoRegistro {
		t.Error("TipoRegistro não é compativel")
	}
	if fim.NumeroDoPedidoDoCliente != arq.Fim.NumeroDoPedidoDoCliente {
		t.Error("NumeroDoPedidoDoCliente não é compativel")
	}
	if fim.QuantidadeTotalDeItens != arq.Fim.QuantidadeTotalDeItens {
		t.Error("QuantidadeTotalDeItens não é compativel")
	}
	if fim.QuantidadeTotalDeUnidades != arq.Fim.QuantidadeTotalDeUnidades {
		fmt.Println("QuantidadeTotalDeUnidades do arquivo = ", arq.Fim.QuantidadeTotalDeUnidades)
		t.Error("QuantidadeTotalDeUnidades não é compativel")
	}
	if fim.ComplementoDoNumeroDoPedido != arq.Fim.ComplementoDoNumeroDoPedido {
		fmt.Println("ComplementoDoNumeroDoPedido do arquivo = ", arq.Fim.ComplementoDoNumeroDoPedido)
		t.Error("ComplementoDoNumeroDoPedido não é compativel")
	}
	if fim.Reservado != arq.Fim.Reservado {
		fmt.Println("reservado do arquivo = ", arq.Fim.Reservado)
		t.Error("Reservado não é compativel")
	}

}
