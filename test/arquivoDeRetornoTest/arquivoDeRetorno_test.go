package arquivoDeRetornoTest

import (
	"fmt"
	"layout-itss/arquivoDeRetorno"
	"testing"
	"time"
)

//Exemplo
//100000000000000007017092018016113000            00000000000000       50899293001645
//207896658002687000000001030
//207896658008740000000000130
//300000000007000002000000000200000000000000

func TestCabecalhoRetorno (t *testing.T) {

	//100000000000000007017092018016113000            00000000000000       50899293001645

	data := time.Date(2018, 9,17,16,11,30,0, time.UTC)
	cabecalho := arquivoDeRetorno.GetCabecalhoRetorno("00000", "000000000070",data,"0",data,"", 0,"000000000000      ","", "50899293001645" )

	if cabecalho != "100000000000000007017092018016113000            00000000000000       50899293001645" {
		fmt.Println("cabecalho", cabecalho)
		t.Error("cabecalho não é compativel")

	}

}

func TestItensRetorno (t *testing.T) {

	//207896658002687000000001030

	itens := arquivoDeRetorno.GetItensRetorno(7896658002687, 0,10,30)

	if itens != "207896658002687000000001030             " {
		fmt.Println("itens", itens)
		t.Error("itens não é compativel")

	}

}

func TestFimRetorno (t *testing.T) {

	//300000000007000002000000000200000000000000

	fim := arquivoDeRetorno.GetFimRetorno("000000000070",2,0,2, 0)

	if fim != "300000000007000002000000000200000000000000                            " {
		fmt.Println("fim", fim)
		t.Error("fim não é compativel")

	}

}