package arquivoDePedido

import gerador_layouts_posicoes "bitbucket.org/infarma/gerador-layouts-posicoes"

type Fim struct {

	TipoRegistro    	   	 	  string `json:"TipoRegistro"`
	NumeroDoPedidoDoCliente  	  string `json:"NumeroDoPedidoDoCliente"`
	QuantidadeTotalDeItens		  int32 `json:"QuantidadeTotalDeItens"`
	QuantidadeTotalDeUnidades 	  float64 `json:"QuantidadeTotalDeUnidades"`
	ComplementoDoNumeroDoPedido   string `json:"ComplementoDoNumeroDoPedido"`
	Reservado            		  string `json:"Reservado"`

}

func (f *Fim) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	tamanho := len(fileContents)
	//Passo as posicoes referentes a esse struct

	if(tamanho == 28 ){

		posicaoParaValor.Posicoes = PosicoesFimIncompleto
	} else if (tamanho <= 31){

		posicaoParaValor.Posicoes = PosicoesFim31
	} else if (tamanho <= 80){

		posicaoParaValor.Posicoes = PosicoesFimCompleto
	}


	err = posicaoParaValor.ReturnByType(&f.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&f.NumeroDoPedidoDoCliente, "NumeroDoPedidoDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&f.QuantidadeTotalDeItens, "QuantidadeTotalDeItens")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&f.QuantidadeTotalDeUnidades, "QuantidadeTotalDeUnidades")
	if err != nil {
		return err
	}


	if(tamanho > 28) {
		err = posicaoParaValor.ReturnByType(&f.ComplementoDoNumeroDoPedido, "ComplementoDoNumeroDoPedido")
		if err != nil {
			return err
		}
	}

	if(tamanho > 31) {
		err = posicaoParaValor.ReturnByType(&f.Reservado, "Reservado")
		if err != nil {
			return err
		}
	}

	return err
}

var PosicoesFimCompleto = map[string]gerador_layouts_posicoes.Posicao{

	"TipoRegistro":                {0,1,0},
	"NumeroDoPedidoDoCliente":     {1,13,0},
	"QuantidadeTotalDeItens":      {13,18,0},
	"QuantidadeTotalDeUnidades":   {18,28,2},
	"ComplementoDoNumeroDoPedido": {28,31,0},
	"Reservado":                   {31,80,0},

}

var PosicoesFimIncompleto = map[string]gerador_layouts_posicoes.Posicao{

	"TipoRegistro":                {0,1,0},
	"NumeroDoPedidoDoCliente":     {1,13,0},
	"QuantidadeTotalDeItens":      {13,18,0},
	"QuantidadeTotalDeUnidades":   {18,28,2},
}

var PosicoesFim31 = map[string]gerador_layouts_posicoes.Posicao{

	"TipoRegistro":                {0,1,0},
	"NumeroDoPedidoDoCliente":     {1,13,0},
	"QuantidadeTotalDeItens":      {13,18,0},
	"QuantidadeTotalDeUnidades":   {18,28,2},
	"ComplementoDoNumeroDoPedido": {28,31,0},

}