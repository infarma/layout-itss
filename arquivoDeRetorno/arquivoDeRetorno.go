package arquivoDeRetorno

import (
	"fmt"
	"time"
)

func GetCabecalhoRetorno( CodigoDoCliente string, NumeroDoPedidoDoCliente string, DataDoPedido time.Time, CondicaoDePagamento string, HoraDoProcessamento time.Time, NumeroDoPedidoDaDistribuidora string, SituacaoDoFechamentoDoPedido int32, Reservado string, PedidoConsulta string, CNPJ string  ) string{


	cabecalho := "1"
	cabecalho += fmt.Sprintf("%06s", CodigoDoCliente)
	cabecalho += fmt.Sprintf("%012s", NumeroDoPedidoDoCliente)
	cabecalho += DataDoPedido.Format("02012006")
	cabecalho += fmt.Sprintf("%-1s", CondicaoDePagamento)
	cabecalho += HoraDoProcessamento.Format("15040500")
	cabecalho += fmt.Sprintf("%-12s", NumeroDoPedidoDaDistribuidora )
	cabecalho += fmt.Sprintf("%02d", SituacaoDoFechamentoDoPedido )
	cabecalho += fmt.Sprintf("%-18s", Reservado )
	cabecalho += fmt.Sprintf("%-1s", PedidoConsulta )
	cabecalho += fmt.Sprintf("%0-14s", CNPJ )


	return cabecalho
}

func GetItensRetorno( CodigoDoProduto int64, QuantidadeAtendida int32, QuantidadeNaoAtendida int32, CodigoMotivoRecusa int32) string{


	Itens := "2"
	Itens += fmt.Sprintf("%014d", CodigoDoProduto)
	Itens += fmt.Sprintf("%05d", QuantidadeAtendida )
	Itens += fmt.Sprintf("%05d", QuantidadeNaoAtendida )
	Itens += fmt.Sprintf("%02d", CodigoMotivoRecusa )
	Itens += fmt.Sprintf("%0-13s", "" )

	return Itens
}

func GetFimRetorno( NumeroDoPedidoDoCliente string, QuantidadeTotalDeItens int32, QuantidadeTotalDeItensAtendidos int32, QuantidadeTotalDeItensNaoAtendidos int32, LimiteDeCreditoDisponivelDoCliente int64) string{


	Fim := "3"
	Fim += fmt.Sprintf("%0-12s", NumeroDoPedidoDoCliente )
	Fim += fmt.Sprintf("%05d", QuantidadeTotalDeItens)
	Fim += fmt.Sprintf("%05d", QuantidadeTotalDeItensAtendidos )
	Fim += fmt.Sprintf("%05d", QuantidadeTotalDeItensNaoAtendidos )
	Fim += fmt.Sprintf("%014d", LimiteDeCreditoDisponivelDoCliente )
	Fim += fmt.Sprintf("%0-28s", "" )

	return Fim
}