package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Cabecalho struct {

	TipoRegistro    	   	 	  string `json:"TipoRegistro"`
	CodigoDoCliente  			  string `json:"CodigoDoCliente"`
	NumeroDoPedidoDoCliente 	  string `json:"NumeroDoPedidoDoCliente"`
	DataDoPedido 	  			  int32 `json:"DataDoPedido"`
	CondicaoDePagamento 		  string `json:"CondicaoDePagamento"`
	TipoDeCompra				  string `json:"TipoDeCompra"`
	TipoDeRetorno				  string `json:"TipoDeRetorno"`
	VersaoDoProgama				  string `json:"VersaoDoProgama"`
	CompraIntegral	 			  string `json:"CompraIntegral"`
	ComplementoDoPedido 		  string `json:"ComplementoDoPedido"`
	NumeroDoPedidoDaDistribuidora string `json:"NumeroDoPedidoDaDistribuidora"`
	VersaoDoProgamaPE 			  string `json:"VersaoDoProgamaPE"`
	CodigoDaOferta 			      string `json:"CodigoDaOferta"`
	Prazo 			  			  string `json:"Prazo"`
	PedidoConsulta 			  	  string `json:"PedidoConsulta"`
	CNPJDDoCliente 			  	  string `json:"CNPJDDoCliente"`
}


func (c *Cabecalho) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesHeader

	err = posicaoParaValor.ReturnByType(&c.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoDoCliente, "CodigoDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroDoPedidoDoCliente, "NumeroDoPedidoDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DataDoPedido, "DataDoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CondicaoDePagamento, "CondicaoDePagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TipoDeCompra, "TipoDeCompra")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TipoDeRetorno, "TipoDeRetorno")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.VersaoDoProgama, "VersaoDoProgama")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CompraIntegral, "CompraIntegral")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.ComplementoDoPedido, "ComplementoDoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroDoPedidoDaDistribuidora, "NumeroDoPedidoDaDistribuidora")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroDoPedidoDaDistribuidora, "NumeroDoPedidoDaDistribuidora")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.VersaoDoProgamaPE, "VersaoDoProgamaPE")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoDaOferta, "CodigoDaOferta")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.Prazo, "Prazo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.PedidoConsulta, "PedidoConsulta")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CNPJDDoCliente, "CNPJDDoCliente")
	if err != nil {
		return err
	}

	return err
}

var PosicoesHeader = map[string]gerador_layouts_posicoes.Posicao{

	"TipoRegistro":					 {0, 1, 0},
	"CodigoDoCliente":				 {1, 7, 0},
	"NumeroDoPedidoDoCliente":		 {7, 19, 0},
	"DataDoPedido":					 {19,27, 0},
	"CondicaoDePagamento":			 {27,28, 0},
	"TipoDeCompra":					 {28,29, 0},
	"TipoDeRetorno":				 {29,30, 0},
	"VersaoDoProgama":				 {30,31, 0},
	"CompraIntegral":				 {31,32, 0},
	"ComplementoDoPedido":			 {32,35, 0},
	"NumeroDoPedidoDaDistribuidora": {35,40, 0},
	"VersaoDoProgamaPE":			 {40, 45, 0},
	"CodigoDaOferta":				 {45, 50, 0},
	"Prazo":						 {50, 53, 0},
	"PedidoConsulta":				 {53, 54, 0},
	"CNPJDDoCliente":				 {54, 68, 0},
}
